package com.example.michalwolowiec.meetup.view.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.viewModel.MainActivityViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * this Fragment gets the data from Firestore based on provided UID
 */
public class MyAccountFragment extends Fragment {


    @BindView(R.id.nameTextView)
    TextView nameTextView;
    @BindView(R.id.ageSexTextView)
    TextView ageSexTextView;
    @BindView(R.id.bioTextView)
    TextView bioTextView;
    @BindView(R.id.phoneNumberTextView)
    TextView phoneNumberTextView;

    private String mUserUID;
    public static final String TAG = "MyAccountFragment";

    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Creates new instance of the AccountInfoFragment with a bundle
     * containing the userUID used to store user's data
     * @param userUID user's UID
     * @return instance of a AccountInfoFragment
     */
    public static MyAccountFragment newInstance(String userUID){
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString("UID", userUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get arguments from bundle if are any
        if(getArguments() != null){
            this.mUserUID = getArguments().getString("UID");
            Log.d(TAG, "onCreate: success getting the user, UID: " + mUserUID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);

        ButterKnife.bind(this, view);

        //set the view model
        MainActivityViewModel mva = ViewModelProviders.of(this)
                .get(MainActivityViewModel.class);

        mva.getUserInfo(mUserUID);

        LiveData<DocumentSnapshot> mUserData = mva.getmUserData();

        //when data is obtained populate the fields
        mUserData.observe(this, new Observer<DocumentSnapshot>() {
            @Override
            public void onChanged(@Nullable DocumentSnapshot documentSnapshot) {
                populateFields(documentSnapshot.getString("name"), documentSnapshot.getString("surname"), documentSnapshot.getString("sex"), documentSnapshot.get("age").toString(),
                documentSnapshot.get("phoneNumber").toString(), documentSnapshot.getString("bio"));
            }
        });


        return view;
    }

    /**
     * This sets the Texts to TextView
     * @param name to set
     * @param surname to set
     * @param sex to set
     * @param age to set
     * @param phoneNumber to set
     * @param bio to sset
     */
    private void populateFields(String name, String surname, String sex, String age, String phoneNumber, String bio){

        String nameToSet = name + " " + surname;
        String ageSexToSet = age + " years old, " + sex.toUpperCase();
        String phoneNumberToSet = getResources().getString(R.string.phone_number_show) + " " + phoneNumber;

        nameTextView.setText(nameToSet);
        ageSexTextView.setText(ageSexToSet);
        phoneNumberTextView.setText(phoneNumberToSet);
        bioTextView.setText(bio);


    }

}
