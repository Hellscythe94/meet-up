package com.example.michalwolowiec.meetup.view.fragments;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.model.Event;
import com.example.michalwolowiec.meetup.model.User;
import com.example.michalwolowiec.meetup.viewModel.MainActivityViewModel;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventDetailsFragment extends Fragment {
    public static final String TAG = "EDF";

    @BindView(R.id.evetNameTextView)
    TextView eventNameTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.locationDescriptionTextView)
    TextView locationDescriptionTextView;
    @BindView(R.id.dateTextView)
    TextView dateTextView;
    @BindView(R.id.numberParticipantsTextView)
    TextView numberParticipantsTextView;
    @BindView(R.id.participantsTextView)
    TextView participantsTextView;

    @BindView(R.id.locationBtn)
    Button locationBtn;
    @BindView(R.id.joinBtn)
    Button joinBtn;

    //parameters
    private String mEventId;
    private String mUserUID;

    private Event currentEvent;
    private MainActivityViewModel mva;



    public EventDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param eventId Parameter 1.
     * @return A new instance of fragment EventDetailsFragment.
     */
    public static EventDetailsFragment newInstance(String eventId, String userUID) {
        EventDetailsFragment fragment = new EventDetailsFragment();
        Bundle args = new Bundle();
        args.putString("eventID", eventId);
        args.putString("userUID", userUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEventId = getArguments().getString("eventID");
            mUserUID = getArguments().getString("userUID");
            Log.d(TAG, "EventID: " + mEventId + " UserUID: " + mUserUID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_details, container, false);

        ButterKnife.bind(this, view);

        mva = ViewModelProviders.of(this)
                .get(MainActivityViewModel.class);

        mva.getEventInfo(mEventId);

        LiveData<DocumentSnapshot> eventInfo = mva.getmEventData();

        eventInfo.observe(this, new Observer<DocumentSnapshot>() {
            @Override
            public void onChanged(@Nullable DocumentSnapshot documentSnapshot) {

                currentEvent = documentSnapshot.toObject(Event.class);

                populateFields(currentEvent.getEventTitle(), currentEvent.getShortDescription(), currentEvent.getEventLocationDescription(), currentEvent.getEventLocation(), currentEvent.getEventDate(),
                        currentEvent.getNumberOfParticipants(), currentEvent.getUsersAttending());
            }
        });

        return view;
    }

    @OnClick(R.id.joinBtn)
    public void clickedJoinBtn(){
        ArrayList<String> attending = currentEvent.getUsersAttending();

        if(attending.size() == currentEvent.getNumberOfParticipants()){
            showToast("The event is full");
            return;
        }
        //iterate through the list to see if the user is already signed to the event
        boolean isNotThere = true;
        for(String uid : attending){
            if(uid.equals(mUserUID)) isNotThere = false;
        }

        //if the user is not already signed to the event add him to the array and save the user to the firestore
        //if he is, just show him the toast telling him that he already is attending the event
        if(isNotThere) {
            attending.add(mUserUID);
            mva.saveEventToFirestore(getActivity(), currentEvent.getEventOwner(), currentEvent.getEventTitle(), currentEvent.getShortDescription(), currentEvent.getEventLocationDescription(),
                    currentEvent.getEventLocation(), currentEvent.getEventDate(), currentEvent.getNumberOfParticipants(), attending);
        }
            else {
            showToast("You are already attending this event");
        }

        mva.getEventInfo(mEventId);
    }

    @OnClick(R.id.locationBtn)
    public void clickedLocationBtn(){
        String geoUri = "google.navigation:q=" + currentEvent.getEventLocation().getLatitude() + "," + currentEvent.getEventLocation().getLongitude();
        Log.d(TAG, "geoUri: " + geoUri);
        Uri gmmIntentUri = Uri.parse(geoUri);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    public void populateFields(String title, String description, String locationDescription, GeoPoint location, Date date, int numberOfParticipants, final ArrayList<String> participants){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        dateTextView.setText(dateFormat.format(date));

        eventNameTextView.setText(title);
        descriptionTextView.setText(description);
        locationDescriptionTextView.setText(locationDescription);
        locationBtn.setText(R.string.where);

        numberParticipantsTextView.setText(String.valueOf("People that will be there: " + numberOfParticipants));
        participantsTextView.setText("");

        mva.getUsersInfo();

        LiveData<ArrayList<User>> attendees = mva.getmUsersInfo();
        attendees.observe(this, new Observer<ArrayList<User>>() {
            @Override
            public void onChanged(@Nullable ArrayList<User> users) {
                Log.d(TAG, "onChanged: WSZEDL");
                if(participants != null){
                    for(String participant : participants){
                        for(User user : users){
                            if(participant.equals(user.getUserId())) participantsTextView.append(user.getName() + ", " + user.getAge() + " \n");
                        }
                    }
                }
            }
        });
    }

    private void showToast(String textToShow){
        Toast toast = Toast.makeText(getActivity(), textToShow, Toast.LENGTH_SHORT);
        toast.show();
    }

}
