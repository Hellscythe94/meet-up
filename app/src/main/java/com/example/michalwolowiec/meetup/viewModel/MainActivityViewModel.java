package com.example.michalwolowiec.meetup.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.michalwolowiec.meetup.model.Event;
import com.example.michalwolowiec.meetup.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;

public class MainActivityViewModel extends ViewModel {

    private final static String TAG = "MAVM";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    private MutableLiveData<FirebaseUser> mUser;
    private MutableLiveData<DocumentSnapshot> mUserData;
    private MutableLiveData<ArrayList<User>> mUsersInfo;
    private MutableLiveData<ArrayList<Event>> mEvents;
    private MutableLiveData<DocumentSnapshot> mEventData;


    public MainActivityViewModel() {
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = new MutableLiveData<>();
        mUserData = new MutableLiveData<>();
        mEvents = new MutableLiveData<>();
        mEventData = new MutableLiveData<>();
        mUsersInfo = new MutableLiveData<>();
    }

    /**
     * This method registers users using email and password
     * @param email user Email to register
     * @param password user password to register
     */
    public void registerUser(String email, String password){

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: Registration completed successfully");
                            mUser.setValue(mAuth.getCurrentUser());
                        } else{
                            Log.d(TAG, "onComplete: Registration Complete with a failure");
                        }
                    }
                });
    }

    /**
     * This method send the email verification to the user
     * @param user User to send verification to
     */
    public void sendEmailVerification(FirebaseUser user){

        user.sendEmailVerification()
        .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: Verification Email Send Succesfully");
                } else {
                    Log.d(TAG, "onComplete: Verification Email Send Failure");
                }
            }
        });
    }

    /**
     * This method signs In the user using email and password
     * @param email email to sing in
     * @param password password to sign in
     */
    public void signIn(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: Signed In Succesfully");
                    mUser.setValue(mAuth.getCurrentUser());
                } else {
                    Log.d(TAG, "onComplete: Signed In Failure");
                }
            }
        });
    }

    /**
     * This method is saving user data to a collection named after the user UID
     * When the data is send shows the user an AlertDialog with a button
     * taking him to the login fragment
     * @param uid user's UID
     * @param name user's name
     * @param surname user's surname
     * @param bio user's bio
     * @param phoneNumber user's phone number
     * @param age user's age
     */
    public void saveToFirebaseCloud(Context context, String uid, String name, String surname, String sex, String bio, int phoneNumber, int age){

        User user = new User(uid, name, surname, bio, phoneNumber, sex, age);
        db.collection("users")
                .document(uid)
                .set(user);

        Log.d(TAG, "saveToFirebaseCloud: Successful save to cloud");
        Toast toast = Toast.makeText(context, "Successful write to cloud", Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Save event to the cloud
     * @param context
     * @param eventOwner
     * @param title
     * @param shortDescription
     * @param locationDescription
     * @param location
     * @param eventDate
     * @param numberOfParticipants
     */
    public void saveEventToFirestore(Context context, String eventOwner, String title, String shortDescription, String locationDescription,
                                     GeoPoint location, Date eventDate, int numberOfParticipants, ArrayList<String> participants){

        Event event = new Event(eventOwner, title, shortDescription, locationDescription, location, eventDate, numberOfParticipants, participants);



        db.collection("events")
                .document(eventOwner)
                .set(event);

        Toast toast = Toast.makeText(context, "Succesfull wirte to cloud", Toast.LENGTH_SHORT);
        toast.show();


    }

    /**
     * This method fetches user data from firestore based on provided userUID
     * @param userUID String to fetch the right data
     */
    public void getUserInfo(final String userUID){
        //get the reference to database
        DocumentReference documentReference = db.collection("users").document(userUID);

        //get the data
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()){
                        Log.d(TAG, "onComplete: document exists, data: " + document.getData());
                        mUserData.setValue(document);
                    } else {
                        Log.d(TAG, "onComplete: no document with this uid: " + userUID);
                    }
                } else {
                    Log.d(TAG, "onComplete: task failed with: " + task.getException());
                }
            }
        });
    }

    /**
     * This gets requested users from the firestore
     */
    public void getUsersInfo(){
        final ArrayList<User> users = new ArrayList<>();
            db.collection("users")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                for(DocumentSnapshot documentSnapshot : task.getResult()){
                                    User user = documentSnapshot.toObject(User.class);

                                    users.add(user);
                                }
                                mUsersInfo.setValue(users);
                            }
                        }
                    });
    }

    /**
     * This fetches the event info from Cloud
     * @param userUID event id that is the same as the userID that created it
     */
    public void getEventInfo(final String userUID){
        DocumentReference documentReference = db.collection("events").document(userUID);

        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()){
                        mEventData.setValue(document);
                    } else Log.d(TAG, "onComplete: no document with this uid: " + userUID);
                } else Log.d(TAG, "onComplete: task failed with: " + task.getException());
            }
        });
    }

    /**
     * This fetches all events that are stored in the firebase
     */
    public void getEvents(){
        final ArrayList<Event> events = new ArrayList<>();

        db.collection("events").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
//                                String eventOwner = document.getData().get("eventOwner").toString();
//                                String eventTitle = document.getData().get("eventTitle").toString();
//                                String description = document.getData().get("shortDescription").toString();
//                                String locationDescription = document.getData().get("eventLocationDescription").toString();
//                                GeoPoint location = (GeoPoint) document.getData().get("eventLocation");
//                                Date eventDate = (Date) document.getData().get("eventDate");
//                                int numberOfParticipants = Integer.parseInt(document.getData().get("numberOfParticipants").toString());
//                                ArrayList<String> participants = (ArrayList<String>) document.getData().get("usersAttending");

                                events.add(event);
                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                            mEvents.setValue(events);
                        }
                    }
                });
    }

    public void sendResetPassword(final Context context, String email){
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(context, "Email sent", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    //Getters and Setters

    public MutableLiveData<FirebaseUser> getmUser() {
        return mUser;
    }
    public MutableLiveData<DocumentSnapshot> getmUserData(){
        return mUserData;
    }

    public MutableLiveData<ArrayList<Event>> getmEvents() {
        return mEvents;
    }

    public MutableLiveData<DocumentSnapshot> getmEventData() {
        return mEventData;
    }

    public MutableLiveData<ArrayList<User>> getmUsersInfo() {
        return mUsersInfo;
    }
}
