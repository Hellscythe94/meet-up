package com.example.michalwolowiec.meetup.view.fragments;

import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Fragment fragment);
}
