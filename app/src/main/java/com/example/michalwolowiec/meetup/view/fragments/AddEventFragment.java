package com.example.michalwolowiec.meetup.view.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.viewModel.MainActivityViewModel;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class AddEventFragment extends Fragment {
    public static final int PLACE_PICKER_REQUEST_CODE = 1;

    public static final String TAG = "addEvent";

    SingleDateAndTimePickerDialog dateAndTimePicker;

    @BindView(R.id.input_event_title)
    EditText eventTitleEditText;
    @BindView(R.id.input_event_description)
    EditText eventDescriptionEditText;
    @BindView(R.id.input_event_location_description)
    EditText eventLocationDescriptionEditText;
    @BindView(R.id.input_number_participants)
    EditText eventNumberParticipantsEditText;
    @BindView(R.id.lat_Lng_TextView)
    TextView latLngTextView;
    @BindView(R.id.place_address)
    TextView placeAddressTextView;

    @BindView(R.id.pick_time_button)
    Button pickTimeBtn;
    @BindView(R.id.pick_location_button)
    Button pickLocationBtn;
    @BindView(R.id.save_event_button)
    Button saveEvent;

    private OnFragmentInteractionListener mListener;

    private MainActivityViewModel mva;

    private String mUserUID;

    private Date pickedDate;
    private GeoPoint location;

    public AddEventFragment() {
        // Required empty public constructor
    }

    public static AddEventFragment newInstance(String userUID){
        AddEventFragment fragment = new AddEventFragment();
        Bundle args = new Bundle();
        args.putString("UID", userUID);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * user clicked the "when" button
     */
    @OnClick(R.id.pick_time_button)
    public void clickedPickTime(){
        if(pickedDate == null) pickedDate = new Date();
        dateAndTimePicker = new SingleDateAndTimePickerDialog.Builder(getContext())
                .backgroundColor(Color.WHITE)
                .mainColor(Color.BLUE)
                .defaultDate(pickedDate)
                .mustBeOnFuture()
                .minutesStep(5)
                .title(getResources().getString(R.string.when))
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        pickedDate = date;
                        pickTimeBtn.setText(dateFormat.format(pickedDate));
                    }
                })
                .build();
        dateAndTimePicker.display();
    }

    /**
     * user clicked the "where" button
     */
    @OnClick(R.id.pick_location_button)
    public void clickedPickLocation(){
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    /**
     * user clicked the "save" button
     */
    @OnClick(R.id.save_event_button)
    public void clickedSaveEvent(){
        if(!validateForm()) return;

        //get the data from form
        String title = eventTitleEditText.getText().toString();
        String eventDescription = eventDescriptionEditText.getText().toString();
        String eventLocationDescription = eventLocationDescriptionEditText.getText().toString();
        String participants = eventNumberParticipantsEditText.getText().toString();
        ArrayList<String> attending = new ArrayList<>();

        //save data to firestore
        mva.saveEventToFirestore(getActivity(), mUserUID, title, eventDescription, eventLocationDescription, location, pickedDate, Integer.parseInt(participants), attending);

        mListener.onFragmentInteraction(new MapsFragment());
    }


    /**
     * Gets called when user picked a location from location picker
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AddEventFragment.PLACE_PICKER_REQUEST_CODE){
            if(resultCode == RESULT_OK){

                //get the place
                Place place = PlacePicker.getPlace(getActivity(), data);
                LatLng latLng = place.getLatLng();

                //save location to a GeoPoint to save to firestore
                location = new GeoPoint(latLng.latitude, latLng.longitude);

                //update TextViews
                latLngTextView.setText("Lat: " + latLng.latitude + ", Lng: " + latLng.longitude);
                placeAddressTextView.setText(place.getAddress());
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_event, container, false);

        mva = ViewModelProviders.of(this)
                .get(MainActivityViewModel.class);

        ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            this.mUserUID = getArguments().getString("UID");
            Log.d(TAG, "onCreate: success getting the user, UID: " + mUserUID);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private boolean validateForm() {
        boolean isValid = true;
        String title = eventTitleEditText.getText().toString();
        String eventDescription = eventDescriptionEditText.getText().toString();
        String eventLocationDescription = eventLocationDescriptionEditText.getText().toString();
        String participants = eventNumberParticipantsEditText.getText().toString();

        if(TextUtils.isEmpty(title)){
            eventTitleEditText.setError("Required");
            isValid = false;
        } else {
            eventTitleEditText.setError(null);
        }

        if(TextUtils.isEmpty(eventDescription)){
            eventDescriptionEditText.setError("Required");
            isValid = false;
        } else {
            eventDescriptionEditText.setError(null);
        }

        if(TextUtils.isEmpty(eventLocationDescription)){
            eventLocationDescriptionEditText.setError("Required");
            isValid = false;
        } else {
            eventLocationDescriptionEditText.setError(null);
        }

        if(TextUtils.isEmpty(participants)){
           eventLocationDescriptionEditText.setError("Required");
            isValid = false;
        } else {
            eventNumberParticipantsEditText.setError(null);
        }

        if(location == null){
            isValid = false;
            Toast toast = Toast.makeText(getActivity(), "Pick a location", Toast.LENGTH_SHORT);
            toast.show();
        } else isValid = true;

        try {
            NumberFormat.getInstance().parse(participants);
        } catch (ParseException e) {
            eventNumberParticipantsEditText.setError("Must be a number");
        }

        return isValid;
    }
}
