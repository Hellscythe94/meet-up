package com.example.michalwolowiec.meetup.view.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.model.Event;
import com.example.michalwolowiec.meetup.view.activities.MainActivity;
import com.example.michalwolowiec.meetup.viewModel.MainActivityViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.rx.ObservableFactory;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener,
        GoogleMap.OnInfoWindowClickListener {

    public static final String TAG = "MapsFragment";
    private static final int LOCATION_REQUEST = 2;

    private MapView mapView;
    private GoogleMap mMap;

    private Location mLocation;

    private ArrayList<Event> mEvents;

    private MainActivityViewModel mva;

    LiveData<ArrayList<Event>> events;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.navigationView)
    NavigationView navigationView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.drawerButton)
    ImageButton drawerButton;

    private String mUserUID;
    //callback to the activity implementing this listener
    private OnFragmentInteractionListener mListener;


    public MapsFragment() {
        // Required empty public constructor
    }

    /**
     * Creates new instance of the AccountInfoFragment with a bundle
     * containing the userUID used to store user's data
     * @param userUID user's UID
     * @return instance of a AccountInfoFragment
     */
    public static MapsFragment newInstance(String userUID){
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putString("UID", userUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            this.mUserUID = getArguments().getString("UID");
            Log.d(TAG, "onCreate: success getting the user, UID: " + mUserUID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);


        //Bind views
        ButterKnife.bind(this, rootView);

        mva = ViewModelProviders.of(this)
                .get(MainActivityViewModel.class);

        mva.getEvents();

        events = mva.getmEvents();

        //observe the downloaded events
        events.observe(this, new Observer<ArrayList<Event>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Event> events) {
                mEvents = events;
            }
        });

        navigationView.setNavigationItemSelectedListener(this);

        //prepare the mapView
        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try{
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e){
            e.printStackTrace();
        }

        mapView.getMapAsync(this);


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * User clicked the fab button
     * redirect him to a addEventFragment
     */
    @OnClick(R.id.fab)
    public void clickedFAB(){
        mListener.onFragmentInteraction(AddEventFragment.newInstance(mUserUID));



    }

    @OnClick(R.id.drawerButton)
    public void clickedDrawerButton(){
        drawerLayout.openDrawer(GravityCompat.START);
    }

    //Interfaces methods

    /**
     * This gets called when map is ready
     */
    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(LOCATION_REQUEST)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //ask for permissionns
        String perm = Manifest.permission.ACCESS_FINE_LOCATION;
        if(getContext() != null && EasyPermissions.hasPermissions(getContext(), perm)){
            mMap.setMyLocationEnabled(true);
            mMap.setOnInfoWindowClickListener(this);
        }else{
            EasyPermissions.requestPermissions(getActivity(),"GIBE ME PERMISSION", LOCATION_REQUEST, perm);
        }

        events.observe(this, new Observer<ArrayList<Event>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Event> events) {
                if(mEvents != null){
                    for(Event event : mEvents){
                        LatLng eventLocation = new LatLng(event.getEventLocation().getLatitude(), event.getEventLocation().getLongitude());
                        mMap.addMarker(new MarkerOptions().position(eventLocation).title(event.getEventTitle()).snippet(event.getShortDescription()));
                    }
                }
            }
        });

        Observable<Location> locationObservable = ObservableFactory.from(SmartLocation.with(getActivity()).location());
        locationObservable.subscribe(new io.reactivex.Observer<Location>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Location location) {
                LatLng locationLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraPosition cameraPosition = new CameraPosition.Builder().target(locationLng).zoom(16).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });


    }

    /**
     * This gets called when user clicks and item in navigationView
     * User gets redirected to appropriate fragment based on the item selected
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getTitle().toString()){

            case "My Account":
                MyAccountFragment fragment = MyAccountFragment.newInstance(mUserUID);
                mListener.onFragmentInteraction(fragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case "My Events":

                break;
            //take the user to the login screen, kill the Maps Activity to prevent user backing to it
            case "Logout":
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
                if(getActivity() != null) getActivity().finish();
                break;
        }

        drawerLayout.closeDrawers();
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String markerTitle = marker.getTitle();
        for(Event event : mEvents){
            if(event.getEventTitle().equals(markerTitle)){
                mListener.onFragmentInteraction(EventDetailsFragment.newInstance(event.getEventOwner(), mUserUID));
            }
        }
        Log.d(TAG, "onInfoWindowClick: CLICKED");
    }
}
