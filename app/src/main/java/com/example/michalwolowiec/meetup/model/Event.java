package com.example.michalwolowiec.meetup.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Event {

//    private String eventID; // this may be a String based on how firebase is assigning ID's
    private String eventOwner; // id of the user that made the event |  this may be a String based on how firebase is assigning ID's
    private String eventTitle;
    private String shortDescription; // for map tag snippet
//    private String description;
    private String eventLocationDescription; // Text description of the location e.g CinemaCity Wroclavia
    private GeoPoint eventLocation;
    private Date eventDate;
    private int numberOfParticipants; // number of people that can join the event
    private ArrayList<String> usersAttending; // array of user that sign up for the event

}
