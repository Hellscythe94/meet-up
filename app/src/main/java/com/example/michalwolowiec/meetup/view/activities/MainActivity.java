package com.example.michalwolowiec.meetup.view.activities;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.view.fragments.AccountInfoFragment;
import com.example.michalwolowiec.meetup.view.fragments.LoginFragment;
import com.example.michalwolowiec.meetup.view.fragments.OnFragmentInteractionListener;
import com.example.michalwolowiec.meetup.view.fragments.RegisterFragment;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        LoginFragment loginFragment = new LoginFragment();
        fragmentTransaction.replace(R.id.mainContainer, loginFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Fragment fragment) {

        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if(fragment instanceof RegisterFragment) {
            fragmentTransaction.replace(R.id.mainContainer, fragment);
        }

        if(fragment instanceof LoginFragment){
            fragmentTransaction.replace(R.id.mainContainer, fragment);
        }

        if(fragment instanceof AccountInfoFragment){
            fragmentTransaction.replace(R.id.mainContainer, fragment);
        }

        fragmentTransaction.commit();
    }
}
