package com.example.michalwolowiec.meetup.view.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.michalwolowiec.meetup.R;
import com.example.michalwolowiec.meetup.view.fragments.MapsFragment;
import com.example.michalwolowiec.meetup.view.fragments.MyAccountFragment;
import com.example.michalwolowiec.meetup.view.fragments.OnFragmentInteractionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    public static final String TAG = "MapsActivity";

    private FragmentManager fm;

    private String uid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //get the intent used to start this activity
        //get the uid from thhe intent
        Intent intent =  getIntent();
        uid = intent.getStringExtra("uid");

        Log.d(TAG, "onCreate: uid" + uid);

        fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MapsFragment mapsFragment = MapsFragment.newInstance(uid);
        fragmentTransaction.replace(R.id.mapsContainer, mapsFragment);
        fragmentTransaction.commit();
    }


    /**
     * Used to communicate between fragments and activity
     * @param fragment fragment to be replaced
     */
    @Override
    public void onFragmentInteraction(Fragment fragment) {

        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        if(fragment instanceof MyAccountFragment){
            fragmentTransaction.replace(R.id.mapsContainer, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
    }
}
