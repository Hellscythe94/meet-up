# Meet Up

Meet Up is a school project that I'm doing in a group. Project is moving quite slowly due to the fact that I'm the only one programming it.

## Concept

 This is a social mobile app that lets users create events for other users to participate. E.g Someone wants to go to the cinema but doesn't want to go alone so they make an event on the map stating the event name, time, its location, user limit, and description. Then other users can see the event on the map and sign up for it.

## Instructions to run

Simply import the project into Android Studio and have the lombok plugin installed. Make sure that your phone/emulator has google play services on it because it will crash/won't work properly.

## Resources

Currently projects uses: 

 - [Lombok](https://projectlombok.org/) for faster model class contruction
 - [ButterKnife](http://jakewharton.github.io/butterknife/) to bind views quickly and with less code
 - [Firebase](https://firebase.google.com/) for authentication, Data Base, and Storage
 - [Smart Location Library](https://github.com/mrmans0n/smart-location-lib?utm_source=android-arsenal.com&utm_medium=referral&utm_campaign=1321) for getting user location
 - [SingleDateAndTimePicker](https://github.com/florent37/SingleDateAndTimePicker?utm_source=android-arsenal.com&utm_medium=referral&utm_campaign=4809) for a nice Date and Time picker
 - [EasyPermissions](https://github.com/googlesamples/easypermissions?utm_source=android-arsenal.com&utm_medium=referral&utm_campaign=5862)